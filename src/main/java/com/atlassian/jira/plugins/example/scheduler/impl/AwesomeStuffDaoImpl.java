package com.atlassian.jira.plugins.example.scheduler.impl;

import java.util.List;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.example.scheduler.AwesomeException;
import com.atlassian.jira.plugins.example.scheduler.AwesomeStuff;
import com.atlassian.jira.plugins.example.scheduler.AwesomeStuffDao;

/**
 * Simple memory-based DAO for awesome stuff.
 *
 * @since v1.0
 */
public class AwesomeStuffDaoImpl implements AwesomeStuffDao
{
    private final ActiveObjects ao;

    public AwesomeStuffDaoImpl(final ActiveObjects ao)
    {
        this.ao = ao;
    }

    @Override
    public AwesomeStuff findById(int id) throws AwesomeException
    {
        return ao.get(AwesomeStuff.class, id);
    }

    @Override
    public AwesomeStuff[] findByAll() throws AwesomeException
    {
        return ao.find(AwesomeStuff.class);
    }

    @Override
    public AwesomeStuff create(String name) throws AwesomeException
    {
        if (name == null || name.isEmpty())
        {
            throw new AwesomeException("Please specify a name for your awesome stuff");
        }
        final AwesomeStuff stuff = ao.create(AwesomeStuff.class);
        stuff.setName(name);
        stuff.save();
        return stuff;
    }

    @Override
    public void remove(AwesomeStuff stuff) throws AwesomeException
    {
        ao.delete(stuff);
    }
}
